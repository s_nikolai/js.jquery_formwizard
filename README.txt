js.jquery_formwizard
====================

.. contents::

Introduction
------------

This library packages `FormWizard`_ for `fanstatic`_.

.. _`fanstatic`: http://fanstatic.org
.. _`FormWizard`: https://github.com/thecodemine/formwizard

This requires integration between your web framework and ``fanstatic``,
and making sure that the original resources (shipped in the ``resources``
directory in ``js.jquery_formwizard``) are published to some URL.